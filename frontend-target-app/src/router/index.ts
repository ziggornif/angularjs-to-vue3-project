import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import SignInView from '../views/SignInView.vue'
import SignUpView from '../views/SignUpView.vue'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      component: SignInView
    },
    {
      path: '/home',
      component: HomeView
    },
    {
      path: '/signup',
      component: SignUpView
    }
  ]
})

export default router
