import axios from 'axios'

export default {
  formatDate(date) {
    return new Intl.DateTimeFormat('fr-FR', {
      weekday: 'short',
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    }).format(date)
  },

  createTweet(message, author) {
    return axios
      .post('http://localhost:8080/tweets', {
        message,
        author
      })
      .then((response) => response.data)
  },

  likeTweet(tweetId) {
    return axios
      .post(`http://localhost:8080/tweets/${tweetId}/like-tweet`)
      .then((response) => response.data)
  },

  listTweets() {
    return axios.get('http://localhost:8080/tweets').then((response) => {
      const tweets = []
      for (const tweet of response.data) {
        tweets.push({
          id: tweet.id,
          message: tweet.message,
          author: tweet.author,
          createdAt: this.formatDate(new Date(tweet.created_at)),
          likes: tweet.likes
        })
      }
      return tweets.reverse()
    })
  }
}
