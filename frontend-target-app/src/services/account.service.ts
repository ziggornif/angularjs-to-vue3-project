import axios from 'axios'

export default {
  authenticate(username, password) {
    return axios
      .post('http://localhost:8080/signin', {
        username,
        password
      })
      .then((response) => {
        const token = response.data
        this.setToken(token)
      })
  },

  register(username, password) {
    return axios.post('http://localhost:8080/signup', {
      username,
      password
    })
  },

  setToken(token) {
    localStorage.setItem('auth-token', token)
  },

  getToken() {
    return localStorage.getItem('auth-token')
  },

  isAuthenticated() {
    const token = this.getToken()
    return token && token.length > 0
  },

  getUsername() {
    const token = this.getToken()
    if (token) {
      const user = atob(token).split(':')[0]
      return user
    }
    return null
  },

  logout() {
    localStorage.removeItem('auth-token')
  }
}
