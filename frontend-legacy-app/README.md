# Frontend legacy app

Legacy app code base of the [Frontend hexagonal architecure demo project](https://gitlab.com/thekitchen/frontend-hexagonal-demo)

## Step 1 : init target project (Vue3)

```
npm create vue@latest
```

## Prompts

1. You are a senior Javascript frontend developper with strong skills in AngularJS and VueJS frameworks.
I need you today to help me migrate an AngularJS application to VueJS
My next messages will be parts of the AngularJS application to migrate.


2. This is the signForm component, please migrate it to VueJS 3 :

sign-form.component.ts
```
// @ts-ignore
import template from "./sign-form.template.html?raw";

/**
 *
 * @param $scope
 * @param tweetService
 */
function controller($scope) {
  this.$onInit = () => {
    this.bgImg = {
      "background-image": `url(${this.img})`,
    };
    this.username = "";
    this.password = "";
  };

  this.clearForm = () => {
    this.username = "";
    this.password = "";
  };

  this.handleSubmit = () => {
    $scope.$emit("signSubmited", {
      username: this.username,
      password: this.password,
    });
    this.clearForm();
  };
}

const component = {
  template,
  controller,
  bindings: {
    formTitle: "@",
    btnLabel: "@",
    img: "@",
  },
  transclude: {
    slot: "?slot",
  },
};

component.$inject = ["$scope"];

export default ["signForm", component];

```

sign-form.template.html
```
<article class="grid">
  <div>
    <h2>{{$ctrl.formTitle}}</h2>
    <form ng-submit="$ctrl.handleSubmit()">
      <label htmlfor="username">
        Username
        <input type="text" id="username" ng-model="$ctrl.username" required />
      </label>
      <label htmlfor="password">
        Password
        <input type="password" id="password" ng-model="$ctrl.password" required />
      </label>
      <button class="contrast">{{$ctrl.btnLabel}}</button>
      <div ng-transclude="slot"></div>
    </form>
  </div>
  <div class="sign-img" ng-style="$ctrl.bgImg"></div>
</article>
```

3. next, let's migrate the layout component that use ng-transclude :

honk-layout.component.ts
```
// @ts-ignore
import template from "./honk-layout.template.html?raw";

/**
 *
 * @param $scope
 * @param tweetService
 */
function controller() {
  this.$onInit = () => {};
}

const component = {
  template,
  controller,
  bindings: {},
  transclude: {
    header: "?slotHeader",
    main: "slotMain",
  },
};

export default ["honkLayout", component];

```

honk-layout.template.html
```
<div class="container">
  <header>
    <nav>
      <ul>
        <li>
          <h1>
            <a href="#/">
              <img src="../../assets/logo.svg" alt="" /> Honk
            </a>
          </h1>
        </li>
      </ul>
      <div ng-transclude="header"></div>
    </nav>
  </header>
  <main><div ng-transclude="main"></div></main>
</div>
```

4. migrate the signin view that use the SignForm.vue component created before :

signin.controller.ts
```
/**
 * Sign in controller
 *
 * @param $scope
 * @param $cookies
 * @param $window
 * @param accountService
 */
function signInController($scope, $window, accountService) {
  /**
   * Authenticate method
   *
   * @param event
   * @param data
   */
  function authenticate(event, data) {
    accountService.authenticate(data.username, data.password).then(() => {
      // eslint-disable-next-line no-param-reassign
      $window.location.href = "#/home";
    });
  }

  $scope.$on("signSubmited", authenticate);
}

signInController.$inject = ["$scope", "$window", "accountService"];

export default ["signInController", signInController];

```

signin.template.html
```
<honk-layout>
  <slot-main>
    <sign-form
      form-title="Sign in"
      btn-label="Login"
      img="https://source.unsplash.com/uymG7UVPXpI/1000x1200"
      username="{{$scope.username}}"
      password="{{$scope.password}}"
    >
      <slot>
        <p class="sign-option">
          Don't have an account yet?
          <a href="#/signup">Register now</a>
        </p>
      </slot>
    </sign-form>
  </slot-main>
</honk-layout>
```

5. migrate also the signup view that use the SignForm.vue component created before :

signup.controller.ts
```
/**
 * Sign up controller
 *
 * @param $scope
 * @param $window
 * @param accountService
 */
function signUpController($scope, $window, accountService) {
  /**
   * Register method
   *
   * @param event
   * @param data
   */
  function register(event, data) {
    accountService.register(data.username, data.password).then(() => {
      // eslint-disable-next-line no-param-reassign
      $window.location.href = "#/";
    });
  }

  $scope.$on("signSubmited", register);
}

signUpController.$inject = ["$scope", "$window", "accountService"];

export default ["signUpController", signUpController];

```

signup.template.html
```
<honk-layout>
  <slot-main>
    <sign-form
      form-title="Register"
      btn-label="Register"
      img="https://source.unsplash.com/uymG7UVPXpI/1000x1200"
      username="{{$scope.username}}"
      password="{{$scope.password}}"
    >
      <slot>
        <p class="sign-option">
          Already have login and password?
          <a href="#/signin">Sign in</a>
        </p>
      </slot>
    </sign-form>
  </slot-main>
</honk-layout>
```

6. now, migrate the create tweet component :

create-tweet.component.ts
```
// @ts-ignore
import template from "./create-tweet.template.html?raw";

/**
 *
 * @param $scope
 */
function controller($scope) {
  this.$onInit = () => {
    this.message = "";
  };

  this.clearForm = () => {
    this.message = "";
  };

  this.submitTweet = () => {
    $scope.$emit("tweetCreated", { message: this.message });
    $scope.$apply(this.clearForm());
  };
}

controller.$inject = ["$scope"];

const component = {
  template,
  controller,
  bindings: {},
};

export default ["createTweet", component];

```

create-tweet.template.html
```
<form ng-submit="$ctrl.submitTweet()">
  <label htmlFor="message">
    Message
    <textarea id="message" ng-model="$ctrl.message" placeholder="What's happening?" required></textarea>
  </label>
  <button>Honk 🚀</button>
</form>
```


7. now, migrate the tweet card component :

tweet-card.component.ts
```
// @ts-ignore
import template from "./tweet-card.template.html?raw";

/**
 *
 * @param $scope
 * @param createTweetAPI
 * @param twitterAPI
 */
function controller($scope) {
  this.likeTweet = () => {
    $scope.$emit("tweetLiked", this.tweetId);
  };
}

controller.$inject = ["$scope"];

const component = {
  template,
  controller,
  bindings: {
    tweetId: "@",
    author: "@",
    createdAt: "@",
    message: "@",
    likes: "@",
  },
};

export default ["tweetCard", component];

```

tweet-card.template.html
```
<div class="tweet-card">
  <div style="display: 'none'">
    <slot name="title" />
  </div>
  <div class="tweet-card__author">
    {{$ctrl.author}} - {{$ctrl.createdAt}}
  </div>
  <p>{{$ctrl.message}}</p>
  <button class="tweet-card__like-button outline" ng-click="$ctrl.likeTweet()">
    {{$ctrl.likes}} ❤️
  </button>
</div>
```

8. migrate the account service :

account.service.ts
```
/**
 *
 * @param $cookies
 */
function accountService($cookies) {
  this.authenticate = (username: string, password: string) => {
    return fetch("http://localhost:8080/signin", {
      method: "POST",
      headers: {
        Accept: "text/plain",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username,
        password,
      }),
    })
      .then((response) => {
        return response.text();
      })
      .then((token) => {
        $cookies.put("auth-token", token);
      });
  };

  this.register = (username: string, password: string) => {
    return fetch("http://localhost:8080/signup", {
      method: "POST",
      headers: {
        Accept: "text/plain",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username,
        password,
      }),
    });
  };

  this.getToken = () => {
    return $cookies.get("auth-token");
  };

  this.isAuthenticated = () => {
    const token = this.getToken();
    return token?.length;
  };

  this.getUsername = () => {
    const token = this.getToken();
    const [user] = atob(token).split(":");
    return user;
  };

  this.logout = () => {
    $cookies.remove("auth-token");
  };
}

export default ["accountService", accountService];

```

9. now we can use the account service in the signin vue  :

signin.controller.ts
```
/**
 * Sign in controller
 *
 * @param $scope
 * @param $cookies
 * @param $window
 * @param accountService
 */
function signInController($scope, $window, accountService) {
  /**
   * Authenticate method
   *
   * @param event
   * @param data
   */
  function authenticate(event, data) {
    accountService.authenticate(data.username, data.password).then(() => {
      // eslint-disable-next-line no-param-reassign
      $window.location.href = "#/home";
    });
  }

  $scope.$on("signSubmited", authenticate);
}

signInController.$inject = ["$scope", "$window", "accountService"];

export default ["signInController", signInController];

```

signin.template.html
```
<honk-layout>
  <slot-main>
    <sign-form
      form-title="Sign in"
      btn-label="Login"
      img="https://source.unsplash.com/uymG7UVPXpI/1000x1200"
      username="{{$scope.username}}"
      password="{{$scope.password}}"
    >
      <slot>
        <p class="sign-option">
          Don't have an account yet?
          <a href="#/signup">Register now</a>
        </p>
      </slot>
    </sign-form>
  </slot-main>
</honk-layout>
```

10. you are not using the username and password sent in the signSubmited event

11. migrate the tweet service :

tweet.service.ts
```
/**
 *
 */
function tweetService() {
  /**
   * Generate output date
   *
   * @param {Date} date input date
   * @returns {string} output
   */
  function formatDate(date: Date): string {
    return new Intl.DateTimeFormat("fr-FR", {
      weekday: "short",
      year: "numeric",
      month: "short",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    }).format(date);
  }

  this.createTweet = (message: string, author: string) => {
    return fetch("http://localhost:8080/tweets", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        message,
        author,
      }),
    }).then((response) => {
      return response.json();
    });
  };

  this.likeTweet = (tweetId: string) => {
    return fetch(`http://localhost:8080/tweets/${tweetId}/like-tweet`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((response) => {
      return response.json();
    });
  };

  this.listTweets = () => {
    return fetch("http://localhost:8080/tweets")
      .then((response) => {
        return response.json();
      })
      .then((jsonResp) => {
        const tweets = [];
        for (const tweet of jsonResp) {
          tweets.push({
            id: tweet.id,
            message: tweet.message,
            author: tweet.author,
            createdAt: formatDate(new Date(tweet.created_at)),
            likes: tweet.likes,
          });
        }
        return tweets.reverse();
      });
  };
}

export default ["tweetService", tweetService];

```
12. now we can migrate the home view :

home.controller.ts
```
/**
 *
 * @param $scope
 * @param $window
 * @param $document
 * @param $cookies
 * @param accountService
 * @param tweetService
 */
function homeController(
  $scope,
  $window,
  $cookies,
  accountService,
  tweetService
) {
  /**
   *
   */
  $scope.logout = () => {
    accountService.logout();
    // eslint-disable-next-line no-param-reassign
    $window.location.href = "#/signin";
  };

  /**
   *
   */
  function getTweets() {
    tweetService.listTweets().then((tweets) => {
      $scope.$apply(() => {
        $scope.tweets = tweets;
      });
    });
  }

  /**
   *
   * @param event
   * @param data
   */
  function createTweet(event, data) {
    tweetService.createTweet(data.message, $scope.username).then(() => {
      return getTweets();
    });
  }

  /**
   *
   * @param event
   * @param data
   */
  function likeTweet(event, data) {
    tweetService.likeTweet(data).then((tweetLiked) => {
      const tIndex = $scope.tweets.findIndex(
        (tweet) => tweet.id === tweetLiked.id
      );
      $scope.$apply(() => {
        $scope.tweets[tIndex].likes = tweetLiked.likes;
      });
    });
  }

  if (!accountService.isAuthenticated()) {
    // eslint-disable-next-line no-param-reassign
    $window.location.href = "#/";
  }

  $scope.username = accountService.getUsername();
  getTweets();

  $scope.$on("tweetCreated", createTweet);
  $scope.$on("tweetLiked", likeTweet);
}

homeController.$inject = [
  "$scope",
  "$window",
  "$cookies",
  "accountService",
  "tweetService",
];

export default ["homeController", homeController];

```

home.template.html
```
<honk-layout>
  <pre>{{tweets | json}}</pre>
  <slot-header>
    <div>
      <ul>
        <li>{{username}}</li>
        <li>
          <button ng-click="logout()" class="outline">
            Logout
          </button>
        </li>
      </ul>
    </div>
  </slot-header>
  <slot-main>
    <article>
      <header>
        <create-tweet
          username="username"
        ></create-tweet>
      </header>
      <tweet-card
        ng-repeat="tweet in tweets track by tweet.id"
        tweet-id="{{tweet.id}}"
        author="{{tweet.author}}"
        message="{{tweet.message}}"
        created-at="{{tweet.createdAt}}"
        likes="{{tweet.likes}}"
      />
    </article>
  </slot-main>
</honk-layout>
```

13. now we can use the account service in the signup vue, do not forget to use data from the event  :

signup.controller.ts

```
/**
 * Sign up controller
 *
 * @param $scope
 * @param $window
 * @param accountService
 */
function signUpController($scope, $window, accountService) {
  /**
   * Register method
   *
   * @param event
   * @param data
   */
  function register(event, data) {
    accountService.register(data.username, data.password).then(() => {
      // eslint-disable-next-line no-param-reassign
      $window.location.href = "#/";
    });
  }

  $scope.$on("signSubmited", register);
}

signUpController.$inject = ["$scope", "$window", "accountService"];

export default ["signUpController", signUpController];

```

14. now write the vue router from the angularjs one

```
// @ts-ignore
import appTemplate from "./controllers/app.template.html?raw";
// @ts-ignore
import homeTemplate from "./controllers/home.template.html?raw";
// @ts-ignore
import signInTemplate from "./controllers/signin.template.html?raw";
// @ts-ignore
import signUpTemplate from "./controllers/signup.template.html?raw";

/**
 *
 * @param $routeProvider
 * @param $locationProvider
 */
function config($routeProvider, $locationProvider) {
  $locationProvider.hashPrefix("");
  $routeProvider
    .when("/", {
      template: appTemplate,
      controller: "appController",
      name: "home",
    })
    .when("/home", {
      template: homeTemplate,
      controller: "homeController",
      name: "home",
    })
    .when("/signin", {
      template: signInTemplate,
      controller: "signInController",
      name: "signin",
    })
    .when("/signup", {
      template: signUpTemplate,
      controller: "signUpController",
      name: "signup",
    });
}

config.$inject = ["$routeProvider", "$locationProvider"];

export default config;

```

