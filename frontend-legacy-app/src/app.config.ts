// @ts-ignore
import appTemplate from "./controllers/app.template.html?raw";
// @ts-ignore
import homeTemplate from "./controllers/home.template.html?raw";
// @ts-ignore
import signInTemplate from "./controllers/signin.template.html?raw";
// @ts-ignore
import signUpTemplate from "./controllers/signup.template.html?raw";

/**
 *
 * @param $routeProvider
 * @param $locationProvider
 */
function config($routeProvider, $locationProvider) {
  $locationProvider.hashPrefix("");
  $routeProvider
    .when("/", {
      template: appTemplate,
      controller: "appController",
      name: "home",
    })
    .when("/home", {
      template: homeTemplate,
      controller: "homeController",
      name: "home",
    })
    .when("/signin", {
      template: signInTemplate,
      controller: "signInController",
      name: "signin",
    })
    .when("/signup", {
      template: signUpTemplate,
      controller: "signUpController",
      name: "signup",
    });
}

config.$inject = ["$routeProvider", "$locationProvider"];

export default config;
