/**
 *
 * @param $scope
 * @param $window
 * @param $document
 * @param $cookies
 * @param accountService
 * @param tweetService
 */
function homeController(
  $scope,
  $window,
  $cookies,
  accountService,
  tweetService
) {
  /**
   *
   */
  $scope.logout = () => {
    accountService.logout();
    // eslint-disable-next-line no-param-reassign
    $window.location.href = "#/signin";
  };

  /**
   *
   */
  function getTweets() {
    tweetService.listTweets().then((tweets) => {
      $scope.$apply(() => {
        $scope.tweets = tweets;
      });
    });
  }

  /**
   *
   * @param event
   * @param data
   */
  function createTweet(event, data) {
    tweetService.createTweet(data.message, $scope.username).then(() => {
      return getTweets();
    });
  }

  /**
   *
   * @param event
   * @param data
   */
  function likeTweet(event, data) {
    tweetService.likeTweet(data).then((tweetLiked) => {
      const tIndex = $scope.tweets.findIndex(
        (tweet) => tweet.id === tweetLiked.id
      );
      $scope.$apply(() => {
        $scope.tweets[tIndex].likes = tweetLiked.likes;
      });
    });
  }

  if (!accountService.isAuthenticated()) {
    // eslint-disable-next-line no-param-reassign
    $window.location.href = "#/";
  }

  $scope.username = accountService.getUsername();
  getTweets();

  $scope.$on("tweetCreated", createTweet);
  $scope.$on("tweetLiked", likeTweet);
}

homeController.$inject = [
  "$scope",
  "$window",
  "$cookies",
  "accountService",
  "tweetService",
];

export default ["homeController", homeController];
