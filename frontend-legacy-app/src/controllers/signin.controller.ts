/**
 * Sign in controller
 *
 * @param $scope
 * @param $cookies
 * @param $window
 * @param accountService
 */
function signInController($scope, $window, accountService) {
  /**
   * Authenticate method
   *
   * @param event
   * @param data
   */
  function authenticate(event, data) {
    accountService.authenticate(data.username, data.password).then(() => {
      // eslint-disable-next-line no-param-reassign
      $window.location.href = "#/home";
    });
  }

  $scope.$on("signSubmited", authenticate);
}

signInController.$inject = ["$scope", "$window", "accountService"];

export default ["signInController", signInController];
