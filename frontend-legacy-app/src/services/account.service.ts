/**
 *
 * @param $cookies
 */
function accountService($cookies) {
  this.authenticate = (username: string, password: string) => {
    return fetch("http://localhost:8080/signin", {
      method: "POST",
      headers: {
        Accept: "text/plain",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username,
        password,
      }),
    })
      .then((response) => {
        return response.text();
      })
      .then((token) => {
        $cookies.put("auth-token", token);
      });
  };

  this.register = (username: string, password: string) => {
    return fetch("http://localhost:8080/signup", {
      method: "POST",
      headers: {
        Accept: "text/plain",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username,
        password,
      }),
    });
  };

  this.getToken = () => {
    return $cookies.get("auth-token");
  };

  this.isAuthenticated = () => {
    const token = this.getToken();
    return token?.length;
  };

  this.getUsername = () => {
    const token = this.getToken();
    const [user] = atob(token).split(":");
    return user;
  };

  this.logout = () => {
    $cookies.remove("auth-token");
  };
}

export default ["accountService", accountService];
