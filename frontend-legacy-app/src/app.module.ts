import angular from "angular";
import "angular-route";

import "./components/components.module";
import "./services/services.module";

import config from "./app.config";
import appController from "./controllers/app.controller";
import homeController from "./controllers/home.controller";
import signInController from "./controllers/signin.controller";
import signUpController from "./controllers/signup.controller";

angular
  .module("myApp", ["ngRoute", "ngCookies", "components", "services"])
  .controller(...appController)
  .controller(...homeController)
  .controller(...signInController)
  .controller(...signUpController)
  .config(config)
  .run();
