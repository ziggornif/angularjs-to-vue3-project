// @ts-ignore
import template from "./tweet-card.template.html?raw";

/**
 *
 * @param $scope
 * @param createTweetAPI
 * @param twitterAPI
 */
function controller($scope) {
  this.likeTweet = () => {
    $scope.$emit("tweetLiked", this.tweetId);
  };
}

controller.$inject = ["$scope"];

const component = {
  template,
  controller,
  bindings: {
    tweetId: "@",
    author: "@",
    createdAt: "@",
    message: "@",
    likes: "@",
  },
};

export default ["tweetCard", component];
